/* 
 * File:   dummyblink.c
 * Author: Pedro Fonseca
 *
 * Dummy program to test downloading programs to Digilent's Max32 board. 
 * 
 * Causes LD4 to blink. 
 * 
 * Created on 31 de Janeiro de 2019, 17:33
 * 
 */
#include "config_bits.h"

#include <xc.h>
#include <stdint.h>

void delayms(uint32_t maxval);

int main(int argc, char** argv) {

    /* Bit RA3 is output */
    TRISAbits.TRISA3 = 0; 

    /* Start with LED ON */
    PORTAbits.RA3 = 1;

    /* Timers */
    
    T1CONbits.ON = 0;
    
    T1CONbits.TGATE = 0;
    T1CONbits.TCS = 0;
    
    T1CONbits.TCKPS = 0b10;
    
    T1CONbits.TSYNC = 0;
    
    PR1 = 625;
    
    T1CONbits.ON = 1;
    
  
    while (1) {
        
        delayms(1000);
        PORTAbits.RA3 = 0;
  
        delayms(1000);
        PORTAbits.RA3 = 1;

    }

    return (EXIT_SUCCESS);
}

void delayms(uint32_t maxval)
{   
    uint32_t contador;
    
    for (contador = 0; contador < maxval; contador++) {
        
        IFS0bits.T1IF = 0; /* Desativar a flag */
        
        while (IFS0bits.T1IF == 0) {
            
        }
    
    }
    
        return;
    
}
